


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import entidades.Editora;
import entidades.Funcionario;
import entidades.Jornal;
import entidades.Livro;
import entidades.Produto;
import entidades.Revista;
import java.util.Date;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
public class Main {
    public static void main(String[] args) {
        
//        Funcionario f = new Funcionario("03738441310", "Izaquiel", "1234", "zef", "123", null);
        Editora e = new Editora("Ideal");
        Produto p = new Revista("1", "Veja",e , 123);
        Produto p2 = new Jornal(new Date(), 12, 211);
        Produto p3 = new Livro("321", "A Bela e a Fera", 3212);
         
        
        Dao<Produto> dao = new Dao<>();
        dao.salvar(e);
        dao.salvar(p);
        dao.salvar(p2);
        dao.salvar(p3);
        
    }
}
