/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Izaquiel
 */
@Entity
public class Venda implements Serializable{
    @Id
    @GeneratedValue
    private long id;
    @Temporal(TemporalType.DATE)
    private Date dataVenda;
    
    private Double valor;
    
    @OneToMany(cascade = CascadeType.MERGE)
    private List<Produto> produtos = new ArrayList<>();
    
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumns({@JoinColumn(name = "FuncionarioId", referencedColumnName = "id"),@JoinColumn(name = "FuncionarioCpf", referencedColumnName = "cpf")})
    private Funcionario funcionario;    
    
    public Venda() {
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    } 

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDataVenda() {
        return dataVenda;
    }

    public void setDataVenda(Date dataVenda) {
        this.dataVenda = dataVenda;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
    
    
}
