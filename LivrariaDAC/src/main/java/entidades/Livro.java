/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidades;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Izaquiel
 */
@Entity
@NamedQueries({@NamedQuery(name = "buscaLivroPorISBN" , query = "Select l From Livro l Where l.isbn = :isbn"),
@NamedQuery(name = "buscarTodosLivros", query = "Select l From Livro l")})
public class Livro extends Produto{
    private String isbn;
    private String titulo;
    
    @ManyToMany(cascade = CascadeType.MERGE)
    private List<Autor> autores;
    
    public Livro() {
        autores = new ArrayList<>();
    }

    public Livro(String isbn, String titulo, int codProduto) {
        super(codProduto);
        this.isbn = isbn;
        this.titulo = titulo;
    }

    public List<Autor> getAutores() {
        return autores;
    }

    public void setAutores(List<Autor> autores) {
        this.autores = autores;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Override
    public String toString() {
        return "Livro - Título: "+ this.titulo + ", ISBN: " + this.isbn;
    }
    
    
    
}
