/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidades;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Izaquiel
 */
@Entity
@NamedQueries({@NamedQuery(name = "buscaJornal", query = "Select j From Jornal j Where j.codProduto = :cod"),
@NamedQuery(name = "buscaTodosJornais", query = "Select j From Jornal j")})
public class Jornal extends Produto{
    
    @Temporal(TemporalType.DATE)
    private Date dataPublicacao;
    
    private int tiragem;

    public Jornal() {
    }

    public Jornal(Date dataPublicacao, int tiragem, int codProduto) {
        super(codProduto);
        this.dataPublicacao = dataPublicacao;
        this.tiragem = tiragem;
    }

    public Date getDataPublicacao() {
        return dataPublicacao;
    }

    public void setDataPublicacao(Date dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public int getTiragem() {
        return tiragem;
    }

    public void setTiragem(int tiragem) {
        this.tiragem = tiragem;
    }

    @Override
    public String toString() {
        return "Jornal - Codigo: " + this.getCodProduto() + "   Tiragem: " +this.getTiragem();
    }
    
    
}
