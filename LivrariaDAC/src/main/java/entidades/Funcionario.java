/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidades;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author Izaquiel
 */
@Entity
@IdClass(FuncionarioPK.class)
@NamedQueries({@NamedQuery(name = "Login", query = "Select f From Funcionario f where f.login =:login and f.senha = :senha"),
@NamedQuery(name = "buscaFuncionario", query = "Select f From Funcionario f where f.cpf = :cpf"),
@NamedQuery(name = "buscaTodosFuncionarios", query = "Select f From Funcionario f")})
public class Funcionario implements Serializable{
    @Id
    @GeneratedValue
    private long id;
    @Id
    private String cpf;
    private String nome;
    private String matricula;
    private String login;
    private String senha;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "EnderecoId")
    private Endereco endereco;
    
    public Funcionario() {
    }

    public Funcionario(String cpf, String nome, String matricula, String login, String senha, Endereco endereco) {
        this.cpf = cpf;
        this.nome = nome;
        this.matricula = matricula;
        this.login = login;
        this.senha = senha;
        this.endereco = endereco;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public String toString() {
        return "Nome: " + this.nome + "   Matricula: " + this.matricula;
    }
    
    
}
