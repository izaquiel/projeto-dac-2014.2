/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores;

import entidades.Autor;
import java.util.List;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
public class AutorContoller {
    
    private Dao<Autor> dao = new Dao<>();
    
    public List<Autor> listaAutores(){
        return dao.buscarTodos("listaAutores");
    }
    
    public void salvar(Autor a){
        dao.salvar(a);
    }
    
    public void atualizar(Autor a){
        dao.atualizar(a);
    }
}
