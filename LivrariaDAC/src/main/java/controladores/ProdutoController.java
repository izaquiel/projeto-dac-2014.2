/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores;

import entidades.Produto;
import java.util.List;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
public class ProdutoController {
    
    private Dao<Produto> dao = new Dao<>();
    
    public List<Produto> getProdutos(){
        return dao.buscarTodos("buscaTodosProdutos");
    }
    
}
