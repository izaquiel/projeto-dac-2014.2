/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores;

import entidades.Livro;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.NoResultException;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
public class LivroController {
    
    private Dao<Livro> dao = new Dao<Livro>();
    
    public void salvar(Livro l){
        dao.salvar(l);
    }
    
    public void atualizar(Livro l){
        dao.atualizar(l);
    }
    
    public Livro buscaLivro(String isbn){
        Map<String, String> map = new HashMap<>();
        map.put("isbn", isbn);
        try{
            return dao.buscar("buscaLivroPorISBN", map);
        }catch(NoResultException e){
            return null;
        }
    }
    
    public List<Livro> getLivros(){
        return dao.buscarTodos("buscarTodosLivros");
    }
}
