/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controladores;

import entidades.Editora;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.NoResultException;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
public class EditoraContoller {
    
    private Dao<Editora> dao = new Dao<>();
    
    public List<Editora> listaEditoras(){
        return dao.buscarTodos("listaTodasEditoras");
    }
    
    public void salvar(Editora e){
        dao.salvar(e);
    }
    
    public void atualizar(Editora e){
        dao.atualizar(e);
    }
    
    public Editora buscarEditoraPorNome(String nome){
        Map<String, String> map = new HashMap<>();
        map.put("nome", nome.toLowerCase());
        try{
            return dao.buscar("buscaEditoraPorNome", map);
        }catch(NoResultException e){
            return null;
        }
    }
}
