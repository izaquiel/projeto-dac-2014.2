
package controladores;

import entidades.Funcionario;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.NoResultException;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
public class LoginController {
    
    Dao<Funcionario> dao = new Dao<>();
    
    public Funcionario logar(String login, String senha) throws NoResultException{
        Map<String, String> map = new HashMap<>();
        map.put("login", login);
        map.put("senha", senha);
        return dao.buscar("Login", map);
    }
}
