/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import entidades.Revista;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.NoResultException;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
public class RevistaContoller {

    private Dao<Revista> dao = new Dao<>();

    public void salvar(Revista r) {
        dao.salvar(r);
    }

    public void atualizar(Revista r) {
        dao.atualizar(r);
    }

    public Revista buscarRevista(int cod) {
        Map<String, Integer> map = new HashMap<>();
        map.put("cod", cod);
        try {
            return dao.buscar("buscaRevistaPorCod", map);
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public List<Revista> getRevistas(){
        return dao.buscarTodos("buscarTodasRevistas");
    }
}
