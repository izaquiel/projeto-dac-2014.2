/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import entidades.Jornal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.NoResultException;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
public class JornalController {

    private Dao<Jornal> dao = new Dao<Jornal>();

    public void salvar(Jornal j){
        dao.salvar(j);
    }
    
    public void atualizar(Jornal j){
        dao.atualizar(j);
    }
    
    public Jornal buscaJornal(int cod) {
        Map<String, Integer> map = new HashMap<>();
        map.put("cod", cod);
        try {
            return dao.buscar("buscaJornal", map);
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public List<Jornal> getJornais(){
        return dao.buscarTodos("buscaTodosJornais");
    }
}
