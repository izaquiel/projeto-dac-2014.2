/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import entidades.Funcionario;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.NoResultException;
import persistencia.Dao;

/**
 *
 * @author Izaquiel
 */
public class FuncionarioController {

    private Dao<Funcionario> dao = new Dao<>();

    public void salvar(Funcionario f) {
        dao.salvar(f);
    }

    public void atualizar(Funcionario f) {
        dao.atualizar(f);
    }

    public List<Funcionario> getFuncionarios() {
        return dao.buscarTodos("buscaTodosFuncionarios");
    }

    public Funcionario getFuncionarioPorCpf(String cpf) {
        Map<String, String> map = new HashMap<>();
        map.put("cpf", cpf);
        try {
            return dao.buscar("buscaFuncionario", map);
        } catch (NoResultException e) {
            return null;
        }
    }
}
