package persistencia;

import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Izaquiel Cruz
 * @param <T>
 */
public class Dao<T> {

    public void salvar(Object novo) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(novo);
        em.getTransaction().commit();
    }

    public void atualizar(Object o) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(o);
        em.getTransaction().commit();
    }

    public void remover(Object o) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        em.remove(em.merge(o));
        em.getTransaction().commit();
    }

    public T buscar(String namedQuery, Map paramentros) throws NoResultException {
        EntityManager em = EntityManagerUtil.getEntityManager();

        Query consulta = em.createNamedQuery(namedQuery);

        Set<String> chaves = paramentros.keySet();

        for (String chave : chaves) {
            consulta.setParameter(chave, paramentros.get(chave));
        }
        
        return (T) consulta.getSingleResult();

    }

    public List<T> buscarTodos(String namedQuery) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        Query consulta = em.createNamedQuery(namedQuery);

        return consulta.getResultList();

    }

    public void setStatus(String namedQuery, Map paramentros) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        Query consulta = em.createNamedQuery(namedQuery);
        Set<String> chaves = paramentros.keySet();

        for (String chave : chaves) {
            consulta.setParameter(chave, paramentros.get(chave));
        }

        consulta.executeUpdate();

    }

}
