
package persistencia;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Izaquiel
 */
public class EntityManagerUtil {

    private static final EntityManagerFactory emf = Persistence.
            createEntityManagerFactory("com.mycompany_LivrariaDAC_jar_1.0-SNAPSHOTPU");
    
    public static EntityManager getEntityManager(){
        return emf.createEntityManager();
    }
}
